This is a custom packaging of the tcsh c shell specifically for alpine
linux. This package was created due to an incompatibility in tcsh with
alpine that causes an out of memory crash. [The patch](https://github.com/openwrt/packages/blob/master/utils/tcsh/patches/001-sysmalloc.patch) creates a version of tcsh
that is at least bootable. There is an [ongoing effort](https://github.com/alpinelinux/aports/pull/3302) to get this
fix merged into the tcsh in the alpine apk package repository, however,
there is still work to be done. Should that be completed, this repository
would be irrelevant.
